# 第一章 知识点杂谈

## 1.1 概述

> 本章节主要记录自己的一些零零散散理解，想到什么就记下来，有的是之前自己的一些误解，现在记录一下。

## 1.2 内存的理解

1. I.MX6U-ALPHA 开发 板 上 的 DDR3 地 址 范 围 是 0X80000000~0XA0000000 (512M)；

2. UBOOT的链接地址为<font color=red>0X87800000</font>，意思是说：程序上电后，uboot的代码会被搬移到0X87800000处，所以我们的裸机代码也链接到这个地方；

3. Linux镜像文件 zImage下载到开发板DRAM的<font color=red>0x80800000</font>;设备树的地址为<font color=red>0x83000000</font>

5. 裸机的栈顶指针定位到<font color=red>0x80200000</font>

    ~~~c
    .global _start
    
    _start:
        /* 设置处理器进入SVC模式 */
        mrs r0, cpsr        /* 读取cpsr到r0*/
        bic r0, r0, #0x1f   /* 清除cpsr的bit4-0*/
        orr r0, r0, #0x13   /* 使用SVC模式*/
        msr cpsr, r0        /* 将r0写入到cpsr*/
    
        /* 设置SP指针 */
        ldr sp, =0x80200000
        b main              /* 跳转到C语言main函数*/
    ~~~

    

6. <font color=green>EMMC仅仅是一个存储器，并不占地址映射，不要把EMMC等价于单片机的内部Flash，两者完全不一样。单片机的内部Flash等价于imx6ull的内部iROM，单片机的SRAM等价于imx6ull的OCRAM，它们都是占用地址总线的</font>，如下图所示：

    <img src="笔记图片/1612259088517.png" alt="1612259088517" style="zoom:80%;" />

6. 在irom中有emmc和nand flash，但是我们无法使用，因为我们不知道具体函数，所以当我们想用nand flash或者emmc去存数据的时候，需要自己实现emmc或nand flash的初始化和读写函数。

7. 

## 1.3 启动方式

> 通过比对**imx6ull**和**s5pv210**两款硬件内部都有irom和iram，可以理解成单片机的flash和sram，它们占总线的地址。启动时，这两款芯片的启动策略是完全不一样的。

### 1.3.1 imx6ull的启动方式

im6ull会通过厂家固化在iROM中的程序，将我们写的bin文件的前4k加载到iram中，注意:<font color=red>bin文件的前4K是配置信息，IVT+BootData+DCD，前4K不是我们的代码，而是配置信息</font>，iRom中的程序会利用这些配置信息，完成初始化时钟，初始化DDR，然后将bin文件加载到DDR中，最后跳转到DDR中去执行程序。

### 1.3.2 S5PV210的启动方式

> s5pv210有两种启动方式，一种是三星厂家推荐的，一种是实际uboot使用的：

* **三星推荐：**bootloader必须小于96KB并大于16KB，假定bootloader为80KB，启动过程是这样子：先开机上电后BL0运行，BL0会加载外部启动设备中的bootloader的前16KB（BL1）到SRAM中去运行，BL1运行时会加载BL2（bootloader中80-16=64KB）到SRAM中（从SRAM的16KB处开始用）去运行；BL2运行时会初始化DDR并且将OS搬运到DDR去执行OS，启动完成。

 <img src="笔记图片/1612342535679.png" alt="1612342535679" style="zoom: 67%;" />

* **uboot方式：**uboot大小随意，假定为200KB。启动过程是这样子：先开机上电后BL0运行，BL0会加载外部启动设备中的uboot的前16KB（BL1）到SRAM中去运行，BL1运行时会初始化DDR，然后将整个uboot搬运到DDR中，然后用一句长跳转（从SRAM跳转到DDR）指令从SRAM中直接跳转到DDR中继续执行uboot直到uboot完全启动。uboot启动后在uboot命令行中去启动OS。注意：<font color=red>uboot的前16K是用户写的代码，会给它链接地址，理论上来讲它要在链接地址处运行，但实际上它被BL0加载到芯片内部SRAM中运行了，所以这前16K代码必须为位置无关码</font>

## 1.4 linux常用指令

### 1.4.1 复制

* 将一个文件夹复制另一个目录下

  ~~~shell
  cp -r ./part3_linux_source/uboot-imx-rel_imx_4.1.15_2.1.0_ga_alientek ./
  // cp mx6ullevk/ -r mx6ull_alientek_emmc
  ~~~

### 1.4.2 解压

* 直接解压

  ~~~shell
  tar -vxjf busybox-1.29.0.tar.bz2
  ~~~

  

#### 1.4.3 搜索

* 对vim打开的文件搜素

  ~~~shell
  
  ~~~

  



  


<div style="page-break-after:always;"></div>  

# 第二章 Makefile和链接脚本

> 定义：
>
> 1. 链接脚本其实是个**规则文件**，它是程序员用来指挥链接器工作的。
>
> 2. 链接器会参考链接脚本，并且使用其中规定的规则来处理.o文件中那些段，将其链接成一个可执行程序。
>
> 3. 链接脚本还可以理解为程序上电后，flash中的程序会被搬移到ddr中的一种划分区域的规则，我们写代码时，可以在程序中使用链接脚本中的地址。

## 2.1 链接脚本
* 链接脚本的关键内容有2部分：段名 + 地址（作为链接地址的内存地址）

* 链接脚本的理解：
    	SECTIONS {}	这个是整个链接脚本
        	. 点号在链接脚本中代表当前位置。
        	= 等号代表赋值

* 范例1：

    ~~~c
    SECTIONS{
    	. = 0X87800000;
    	.text :
    	{
    		obj/start.o 
    		*(.text)
    	}
    	.rodata ALIGN(4) : {*(.rodata*)}     
    	.data ALIGN(4)   : { *(.data) }    
    	__bss_start = .;    
    	.bss ALIGN(4)  : { *(.bss)  *(COMMON) }    
    	__bss_end = .;
    }
    ~~~
~~~


​    
* 范例2

    ~~~c
    SECTIONS
    {
    	. = 0xd0024000;
    	.text : {
    		start.o
    		* (.text)
    	}	
        
    	.data : {
    		* (.data)
    	}
        
    	bss_start = .; 
    	.bss : {
    		* (.bss)
    	}
    	bss_end  = .;	
    }
~~~


​    
    你好，

## 2.2 Makefile





通用版makefile为：

~~~makefile
CROSS_COMPILE 	?= arm-linux-gnueabihf-
TARGET		  	?= int

CC 				:= $(CROSS_COMPILE)gcc
LD				:= $(CROSS_COMPILE)ld
OBJCOPY 		:= $(CROSS_COMPILE)objcopy
OBJDUMP 		:= $(CROSS_COMPILE)objdump

INCDIRS 		:= imx6ul \
				   bsp/clk \
				   bsp/led \
				   bsp/delay  \
				   bsp/beep \
				   bsp/gpio \
				   bsp/key \
				   bsp/exit \
				   bsp/int
				   			   
SRCDIRS			:= project \
				   bsp/clk \
				   bsp/led \
				   bsp/delay \
				   bsp/beep \
				   bsp/gpio \
				   bsp/key \
				   bsp/exit \
				   bsp/int
				   
				   
INCLUDE			:= $(patsubst %, -I %, $(INCDIRS))

SFILES			:= $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.S))
CFILES			:= $(foreach dir, $(SRCDIRS), $(wildcard $(dir)/*.c))

SFILENDIR		:= $(notdir  $(SFILES))
CFILENDIR		:= $(notdir  $(CFILES))

SOBJS			:= $(patsubst %, obj/%, $(SFILENDIR:.S=.o))
COBJS			:= $(patsubst %, obj/%, $(CFILENDIR:.c=.o))
OBJS			:= $(SOBJS) $(COBJS)

VPATH			:= $(SRCDIRS)

.PHONY: clean
	
$(TARGET).bin : $(OBJS)
	$(LD) -Timx6ul.lds -o $(TARGET).elf $^
	$(OBJCOPY) -O binary -S $(TARGET).elf $@
	$(OBJDUMP) -D -m arm $(TARGET).elf > $(TARGET).dis

$(SOBJS) : obj/%.o : %.S
	$(CC) -Wall -nostdlib -c -O2  $(INCLUDE) -o $@ $<

$(COBJS) : obj/%.o : %.c
	$(CC) -Wall -nostdlib -c -O2  $(INCLUDE) -o $@ $<
	
clean:
	rm -rf $(TARGET).elf $(TARGET).dis $(TARGET).bin $(COBJS) $(SOBJS)

	

~~~



<div style="page-break-after:always;"></div>  
# 第三章 启动方式





  <div style="page-break-after:always;"></div>  
# 第四章 裸机开发

## 4.1 时钟设置

> **外部晶振24MHz**
>
> 2. **系统时钟：**选择24M作为系统时钟源，寄存器 CCM_ANALOG_PLL_ARMn ，然后就可以进行倍频PPL；
> 2. **系统时钟PPL2：**默认528M，不可以编程修改，该时钟为大部分外设的根时钟源；
> 3. **PPL3：**默认480M，不可以编程修改；
> 4. **PPL7：**默认480M，不可以编程修改；

## 4.2 中断系统

> * imx6ull为Cortex-A7内核，其实分析时，和Cortex-M3内核一样。只不过M3是NVIC控制器，A7是GIC控制器。
> * 完全可以按照M3的方式理解A7的中断系统，两者都有中断向量表和中断向量表偏移，只不过用语言描述的不一样。

### 4.2.1 原理

​	和STM32一样， Cortex-A7也有中断向量表，<font color=blue>中断向量表是在代码的最前面</font>。当IROM的程序跳转到0x87800000处去运行我们写的第一句代码时，也是执行的<font color=red>Reset_Handler</font>函数，这是我们可以控制的第一个函数。严格意义上来说，我们上电后做的第一件事情就是设置中断向量偏移地址，这样上电后当中断触发时，才可以执行中断向量表的中断。中断向量重定位方式：

~~~c
/* 中断向量重定位设置代码 __set_VBAR((uint32_t)0X87800000)*/
FORCEDINLINE __STATIC_INLINE void __set_VBAR(uint32_t vbar)
{
  __MCR(15, 0, vbar, 12, 0, 0);			// 设置CP15寄存器c15
}
~~~



* 1. Cortex-A7有8个中断向量，<font color=red size=3>中断向量表如下</font>，注意：这里的中断向量表完全类似于stm32的中断向量表，最常用的是Reset_Handler和IRQ_Handler中断：

~~~assembly
/* .global symbol说明
 * 1，.global 是使得连接程序（ld）能够识别符号，而symbol是全局可见的
 * 2， 标号_start是GNU链接器用来指定第一个要执行指令所必须的, 同样的是全局可见的 (并且只能出现在一个模块中)
*/
.global _start		/* 定义 _start 为外部程序可以访问的第一个函数标号 */

_start:
    ldr pc, =Reset_Handler      /* 复位中断服务函数，初始化SP指针、DDR等 */
    ldr pc, =Undefined_Handler  /* 未定义指令中断服务函数，如果指令不能识别的话就会产生此中断 */
    ldr pc, =SVC_Handler       /* SVC，由SWI指令引起的中断 */
    ldr pc, =PreAbort_Handler  /* 预取终止，预取指令的出错的时候会产生此中断 */
    ldr pc, =DataAbort_Handler /* 数据终止，访问数据出错的时候会产生此中断 */
    ldr pc, =NotUsed_Handler   /* 未使用 */
    ldr pc, =IRQ_Handler       /* IRQ中断，芯片内部的外设中断都会引起此中断的发生 */
    ldr pc, =FIQ_Handler       /* FIQ中断，如果需要快速处理中断的话就可以使用此中 */
~~~

* 2. 我们一般写裸机中断只用IRQ_Handler，<font color=blue>Cortex-A内核CPU的所有外部中断都属于这个IQR中断，当任意一个外部中断发生的时候都会触发IRQ中断，触发了IRQ中断后，会执行IRQ中断服务函数，在IRQ中断服务函数里面就可以读取指定的寄存器来判断发生的具体是什么中断，进而根据具体的中断做出相应的处理。</font>比如下图中，左侧任何一个外设中断触发都会触发IRQ中断。

        <img src="笔记图片/1612441337827.png" alt="1612441337827" style="zoom: 67%;" />

  

* 原理：在IRQ_Handler中断服务函数中，需要触发的中断源，然后去执行响应的中断服务函数。


### 4.2.2 中断设置步骤

* **STM32的设置步骤：**

  1. 设置中断分组（SCB->AIRCR），在程序最开始做，可以main（）函数后的第一条语句；

  2. 设置中断优先级（NVIC->IP）

     ~~~c
     NVIC->IP[NVIC_InitStruct->NVIC_IRQChannel] = tmppriority;		// NVIC_InitStruct->NVIC_IRQChannel表示向量表中对应的中断
     ~~~

     

     本质上设置了分组的抢占优先级和子优先级都会计算出来一个优先级，将某个中断可以设置成该优先级；

     > 可以通过中断标号找到该中断，NVIC在内部已经帮我们将中断向量表的中断服务函数和中断标号一一绑定了，不需要我们在额外的设置,如下：
     >
     > ~~~c
     > typedef enum IRQn
     > {
     > /******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
     >   NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                             */
     >   MemoryManagement_IRQn       = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt              */
     >   BusFault_IRQn               = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                      */
     >   。。。。。。
     >   。。。。。。
     >   PendSV_IRQn                 = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                       */
     >   SysTick_IRQn                = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                   */
     > 
     > /******  STM32 specific Interrupt Numbers *********************************************************/
     >   WWDG_IRQn                   = 0,      /*!< Window WatchDog Interrupt                            */
     >   PVD_IRQn                    = 1,      /*!< PVD through EXTI Line detection Interrupt            */
     >   TAMPER_IRQn                 = 2,      /*!< Tamper Interrupt                                     */
     >   RTC_IRQn                    = 3,      /*!< RTC global Interrupt                                 */
     >   。。。。。。
     >   。。。。。。
     > }
     > ~~~

  3. 使能对应的总中断（NVIC->ISER），注意每个bit位表示一个中断

     ~~~c
     /* Enable the Selected IRQ Channels --------------------------------------*/
     NVIC->ISER[NVIC_InitStruct->NVIC_IRQChannel >> 0x05] =
           (uint32_t)0x01 << (NVIC_InitStruct->NVIC_IRQChannel & (uint8_t)0x1F);
     ~~~

  4. 在外设初始化化时，设置对应的中断触发源，每个外设对应的中断触发源不一样，例如，可以配置发送完成触发中断，接收的数据触发中断等，最后别忘了使能该外设；

  5. 当中断触发后，nvic会直接到中断向量表中找到相应的中断，所以，要编写中断向量表里面的中断服务函数；

* **iMX6ULL的设置步骤**（还没看代码验证，估计差不多）：
  
  1. 设置中断优先级分组；
  
      > 比如 I.MX6U 的优先级位数为 5 (32 个优先级)，**GICC_BPR**可设置为2，都是抢占优先级；
      >
  
  2. 设置中断优先级（<font color=red>一个中断ID应该对应一个中断</font>，芯片的中断ID应该是固定的，应该也有一个类似的枚举表）；
  
      > <font color=red>需要先根据中断id找到中断，然后在设置中断优先级**GICD_IPRIORITYR**，imx6ull最多支持32个中断</font>
      >
      > （一）、I.MX6U 支持32个优先级，所以**GICC_PMR**要设置为0b11111000，设置中断优先级个数为32（stm32不需要设置）； 
      >
      > （二）、尽管A7的内核最多支持512个中断，但是，厂家用不了那么多，会做私有定义；
      >
      > 1. 和stm32一样，前32个中断id是固定的（ARM内核定义），如下：
      >
      >     <font color=blue>ID0~ID15：这 16 个 ID 分配给 SGI；</font>
      >
      >     <font color=blue> ID16~ID31：这 16 个 ID 分配给 PPI</font>
      >
      > 2. 后面的128个中断id是厂家定义的，如下：
      >
      > | IRQ    | ID     | 中断源                              | 描述                         |
      > | ------ | ------ | :---------------------------------- | ---------------------------- |
      > | 0      | 32     | boot                                | 用于在启动异常的时候通知内核 |
      > | 1      | 33     | DAP 中断                            | 调试端口访问请求中断         |
      > | 2      | 34     | sdma                                | SDMA 中断                    |
      > | 3      | 35     | tsc                                 | TSC(触摸)中断                |
      > | 4      | 36     | snvs_lp_wrapper<br/>snvs_hp_wrapper | SNVS 中断                    |
      > | 。。。 | 。。。 | 。。。                              | 。。。                       |
      > | 125    | 157    | 无                                  | 保留                         |
      > | 126    | 158    | 无                                  | 保留                         |
      > | 127    | 159    | PMU                                 | PMU中断                      |
  
  3. 使能外设总中断，开启的是IRQ总开关，需要用到通用功能寄存器**CPSR**，然后再使能每个外设中断总的寄存器：
  
      <img src="笔记图片/1612699221558.png" alt="1612699221558" style="zoom:67%;" />
  
      代码为：
  
      ~~~c
      cpsie i             /* 打开IRQ */
      /*  
       * 使能指定的中断
       */
      FORCEDINLINE __STATIC_INLINE void GIC_EnableIRQ(IRQn_Type IRQn)
      {
          /* 获取gic控制器的地址 */
        	GIC_Type *gic = (GIC_Type *)(__get_CBAR() & 0xFFFF0000UL);
        	gic->D_ISENABLER[((uint32_t)(int32_t)IRQn) >> 5] = (uint32_t)(1UL << (((uint32_t)(int32_t)IRQn) & 0x1FUL));
      }
      ~~~
  
      
  
  4. 设置外设中断触发源（每个外设的设置方式不一样）；
  
  5. 当中断触发后，GIC控制器会使程序进入IRQ总中断，在IRQ总中断里面，我们通过CP15寄存器找到相应的触发源，然后去执行相应的中断处理函数；

### 4.2.3 GIC控制器

	* GIC控制器配置的目的，就是为了设置中断；
	* 需要CP15寄存器来找到GIC控制器的基址；

### 4.2.4 cp15寄存器

* <font color=blue>CP15协处理器有 16个32位寄存器c0~c15，通过MCR或MRC的操作组合和实现很多不同功能的寄存器，需要看ARM内核手册，我们设置中断只需要用的4个：</font>

    | cp15寄存器 | 功能                                               |
    | ---------- | -------------------------------------------------- |
    | c0         | 获取imx6ull的使用的ARM内核信息                     |
    | c1         | 系统控制期存器，比如：使能或者禁止 MMU、 I/D Cache |
    | c12        | 设置向量表重定位                                   |
    | c15        | 获取GIC基址                                        |

### 4.2.5 Reset_Handler中断

* 关闭Cache和MMU

~~~assembly
 /* 复位中断服务函数 */
Reset_Handler:

    cpsid i                 /* 关闭IRQ */
    /* 关闭I，D Cache和MMU 
     * 修改SCTLR寄存器，采用读-改-写的方式
     */
    MRC p15, 0, r0, c1, c0, 0 /* 读取SCTLR寄存器的数据到r0寄存器里面*/
    bic r0, r0, #(1 << 12)      /* 关闭I Cache */
    bic r0, r0, #(1 << 11)      /* 关闭分支预测 */
    bic r0, r0, #(1 << 2)       /* 关闭D Cache*/
    bic r0, r0, #(1 << 1)       /* 关闭对齐 */
    bic r0, r0, #(1 << 0)       /* 关闭MMU */
    MCR p15, 0, r0, c1, c0, 0   /* 将R0寄存器里面的数据写入到SCTLR里面*/

#if 0
    /* 设置中断向量偏移 */
    ldr r0, =0x87800000
    dsb
    isb
    MCR p15,0,r0,c12,c0,0  /* 设置VBAR寄存器=0X87800000 */
    dsb
    isb
#endif

.global _bss_start
_bss_start:
    .word __bss_start

.global _bss_end
_bss_end:
    .word __bss_end
    
    /*清除BSS段*/
    ldr r0, _bss_start
    ldr r1, _bss_end
    mov r2, #0
bss_loop:
    stmia r0!, {r2}
    cmp r0, r1      /* 比较R0和R1里面的值 */
    ble bss_loop    /*如果r0地址小于等于r1,继续清除bss段*/


    /* 设置处理器进入IRQ模式 */
    mrs r0, cpsr        /* 读取cpsr到r0*/
    bic r0, r0, #0x1f   /* 清除cpsr的bit4-0*/
    orr r0, r0, #0x12   /* 使用IRQ模式*/
    msr cpsr, r0        /* 将r0写入到cpsr*/
    ldr sp, =0x80600000 /* 设置IRQ模式下的sp*/

    /* 设置处理器进入SYS模式 */
    mrs r0, cpsr        /* 读取cpsr到r0*/
    bic r0, r0, #0x1f   /* 清除cpsr的bit4-0*/
    orr r0, r0, #0x1f   /* 使用SYS模式*/
    msr cpsr, r0        /* 将r0写入到cpsr*/
    ldr sp, =0x80400000 /* 设置SYS模式下的sp*/

    /* 设置处理器进入SVC模式 */
    mrs r0, cpsr        /* 读取cpsr到r0*/
    bic r0, r0, #0x1f   /* 清除cpsr的bit4-0*/
    orr r0, r0, #0x13   /* 使用SVC模式*/
    msr cpsr, r0        /* 将r0写入到cpsr*/
    ldr sp, =0x80200000 /* 设置SVC模式下的sp*/

    cpsie i             /* 打开IRQ */
    b main              /* 跳转到C语言main函数*/

~~~

### 4.2.6 IRQ_Handler中断

这个中断函数，对于A7的内核来说应该是通用的

~~~assembly
IRQ_Handler:
    /* -保护现场 */
	push {lr}					/* 保存IRQ模式下lr地址 */
	push {r0-r3, r12}			/* 保存r0-r3，r12寄存器 */

	mrs r0, spsr				/* 读取spsr状态寄存器 */
	push {r0}					/* 保存spsr状态寄存器 */
	
	/* -获取GIC控制器的基址 */
	mrc p15, 4, r1, c15, c0, 0 /* 从CP15的C0寄存器内的值到R1寄存器中
								* 参考文档ARM Cortex-A(armV7)编程手册V4.0.pdf P49
								* Cortex-A7 Technical ReferenceManua.pdf P68 P138
								*/							
	add r1, r1, #0X2000			/* GIC基地址加0X2000，也就是GIC的CPU接口端基地址 */
	ldr r0, [r1, #0XC]			/* GIC的CPU接口端基地址加0X0C就是GICC_IAR寄存器，
								 * GICC_IAR寄存器保存这当前发生中断的中断号，我们要根据
								 * 这个中断号来绝对调用哪个中断服务函数
								 */
	push {r0, r1}				/* 保存r0,r1 */
	
	cps #0x13					/* 进入SVC模式，允许其他中断再次进去 */
	
	push {lr}					/* 保存SVC模式的lr寄存器 */
	ldr r2, =system_irqhandler	/* 加载C语言中断处理函数到r2寄存器中*/
	blx r2						/* 运行C语言中断处理函数，带有一个参数，保存在R0寄存器中 */

	pop {lr}					/* 执行完C语言中断服务函数，lr出栈 */
	cps #0x12					/* 进入IRQ模式 */
	pop {r0, r1}				
	str r0, [r1, #0X10]			/* 中断执行完成，写EOIR */

	/* 恢复现场 */
	pop {r0}						
	msr spsr_cxsf, r0			/* 恢复spsr */
	/* lr中存储着进入中断前的地址，内核自己存进lr的 */
	pop {r0-r3, r12}			/* r0-r3,r12出栈 */
	pop {lr}					/* lr出栈 */
	subs pc, lr, #4				/* 将lr-4赋给pc */
~~~





## 4.3 基本外设

### 	4.3.1 GPIO

* 1、GPIO设置（输入/输出）

  > 1. GPIO时钟设置，<font color=blue>可以将所有时钟使能IOMUXC_GPR_GPR0、1、2、3、4、5、6</font>（**参考 CCM 章节**）
  > 2. IO复用为GPIO，寄存器 IOMUXC_SW_MUX_CTL_PAD_GPIO1_IO03 （**参考 IOMUXC 章节32**）
  > 3. IO电气属性（上/下拉、速度等），寄存器 IOMUXC_SW_PAD_CTL_PAD_GPIO1_IO03 
  > 4. 设置为输入/输出，GPIO1_GDIR（**参考 GPIOx_DR 章节**）；
  > 5. 设置输出高/低电平，GPIO1_DR（**参考 GPIOx_DR 章节**）；

### 4.3.2 定时器

定时器的设置其实和单片机的设置一样，之前玩过nrf52832，现在完全可以按照之前的逻辑来配置imx6ull的定时器。

* EPIT定时器

    > 类似于单片机定时器外设 Timer。



* GPT 定时器

    > GPT (General Purpose Timer) 定时器可以实现高精度延时，类似于单片机的systick，但是 GPT是imx6ull的一个模块，不是a7内核的芯片自带的。
    >



### 4.3.3 UART外设

### 4.3.4 I2C外设



### 4.3.5 SPI外设



### 4.3.6 LCD屏幕



### 4.3.7 DDR3实验

S5PV210是







  <div style="page-break-after:always;"></div>  
# 第5章 UBOOT和内核移植

## 5.1 初次编译uboot

### 5.1.1 烧写uboot

> 本章节讲解烧写正点原子提供的uboot的步骤，然后

1. 安装 ncurses 库，不然编译uboot会报错  

~~~shell
sudo apt-get install libncurses5-dev
~~~

2. 将下载好的uboot源码（正点原子的uboot）放到ubuntu中，然后解压

~~~shell
tar -vxjf uboot-imx-2016.03-2.1.0-g8b546e4.tar.bz2
~~~

3. 进入第二步解压后的文件夹，编译uboot，有两种方式

第一种，直接在命令行中输入：

~~~shell
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- distclean
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- mx6ull_14x14_ddr512_emmc_defconfig
make V=1 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j12
~~~

第二种，编写脚本mx6ull_alientek_emmc.sh :

~~~shell
#!/bin/bash
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- distclean
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- mx6ull_14x14_ddr512_emmc_defconfig
make V=1 ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -j12
~~~

>   uboot是bootloader的一种，还支持很多架构和外设，比如USB、网络、SD卡等，所以当我们想开启某个功能时，需要配置。配置文件在uboot/configs中。正点原子的配置文件在<font color=red>uboot-imxrel_imx_4.1.15_2.1.0_ga_alientek/configs</font>中。

~~~~shell
chmod 777 mx6ull_alientek_emmc.sh
./mx6ull_alientek_emmc.sh
~~~~

4. 编译完成，就可以烧写到sd卡中，然后启动uboot，在SecureCRT上出现“Hit any key to stop autoboot: ”倒计时的时候按下键盘上的回车键，就会进入到uboot的命令模式，若不按下按键，就会去启动linux内核。

### 5.1.2 uboot常用的命令

* 1. 信息查询命令

        | 命令                            | 功能                                                         |
        | ------------------------------- | ------------------------------------------------------------ |
        | bdinfo                          | 获取DRAM的起始地址和大小、启动参数保存起始地址、波特率、sp(堆栈指针)起始地址等信息 |
        | <font color=red>printenv</font> | 输出**环境变量**信息，我们可以判断环境变量是否正确           |
        | version                         | 查看uboot的版本号                                            |

* 2. 环境变量操作

        | 命令                           | 功能                                                         |
        | ------------------------------ | ------------------------------------------------------------ |
        | <font color=red>setenv</font>  | 设置环境变量，后要加设置的变量信息，<font color=blue>设置环境变量为空，表示删除环境变量</font> |
        | <font color=red>saveenv</font> | 保存环境变量，直接输入saveenv就可以保存                      |

        > 一般环境变量是存放在外部flash中，uboot启动时会将环境变量从flash读取到DRAM中，setenv修改的是DRAM中的环境变量值，saveenv用于保存修改后的环境变量，保存到flash中，若不保存，uboot下一次重启会继续使用以前的环境变量值。 

       注意：有时我们修改的环境变量值可能会有空格，比如 bootcmd、 bootargs等， 这个时候环境变量值就得用单引号括起来，比如下面修改环境变量bootargs的值，解释：“**console=ttymxc0,115200**”、“**root=/dev/mmcblk1p2**”、“**rootwait**”和“**rw**”相当于四组“值”，这四组值都属于bootargs。

    ~~~shell
    setenv bootargs 'console=ttymxc0,115200 root=/dev/mmcblk1p2 rootwait rw'
    saveenv 
    ~~~

* 3. 内存操作命令

        | 命令 | 功能                                                         |
        | ---- | ------------------------------------------------------------ |
        | md   | 用于显示内存值, <font color=red>md[.b, .w, .l] address [# offset（默认加0x，如10是0x10，换成十进制就是16）]</font>,.b表示字节，.w表示2个字节，.l表示4字节 |
        | nm   | 修改指定地址的内存值，如 <font color=red>nm.l 80000000</font>，输完后输入q表示退出 |
        | mm   | 命令也是修改指定地址内存值的，使用mm修改内存值的时候地址会自增，而使用nm的话地址不会自增 |
        | mw   | 用于使用一个指定的数据填充一段内存，<font color=red>mw [.b, .w, .l] address value [count]</font> |
        | cp   | 数据拷贝命令，用于将DRAM中的数据从一段内存拷贝到另一段内存中<font color=red>cp [.b, .w, .l] source target count</font> |
        | cmp  | 用于比较两段内存的数据是否相等 <font color=red>cmp [.b, .w, .l] addr1 addr2 count</font> |

        命令范例：

        ~~~shell
        md.l 80000000 10 // 显示16个4字节
        nm.l 80000000 // 修改内存地址，先按回车，然后修改，改完后，输入q表示退出
        mw.l 80000000 0A0A0A0A 10 // 向0x80000000后的地址填充16个字的0x0A0A0A0A
        cp.l 80000000 80000100 10 // 数据拷贝命令，将0x80000000的数据拷贝到80000100
        cmp.l 80000000 80000100 10 // 比较0x80000000和0x80000100的数据是否相等
        ~~~

* 4. 网络操作命令

  将网线插入开发板ENET2接口，才能进行网络指令测试

  <img src="笔记图片/1612858564236.png" alt="1612858564236" style="zoom:50%;" />

  | 命令 | 功能                                |
  | ---- | ----------------------------------- |
  | ping | 查看开发板网络是否可用，ping ip地址 |
  | dhcp | 用于从路由器获取IP地址              |
  | nfs  | [点击查看功能](./挂载根文件系统.md) |
  | tftp | [点击查看功能](./挂载根文件系统.md) |

* 5. boot指令

  | 命令     | 功能                                                         |
  | -------- | ------------------------------------------------------------ |
  | bootz    | 用于启动DDR中的zImage镜像文件，首先要将zImage存放到DDR中<font color=red>bootz [addr [initrd[:size]] [fdt]]</font>，fdt表示设备树的地址 |
  | bootm    | 兼容bootz功能，添加了可以单独启动zImage，不启动设备树的功能 bootm addr |
  | boot     | <>用来启动Linux系统，<font color=red>只是boot会通过读取环境变量bootcmd来启动Linux系统</font> |
  | bootargs | <font color=red>传递给内核的启动参数</font>                  |
  | bootcmd  | <font color=red>自动启动时执行的命令，有默认值</font>        |
>bootcmd命令理解：
>
>​	bootcmd保存着uboot默认命令，uboot倒计时结束以后就会执行bootcmd中的命令。这些命令一般都是用来启动Linux内核的，比如读取EMMC或者NAND Flash中的 Linux 内核镜像文件和设备树文件到 DRAM 中，然后启动 Linux 内核。可
>以在 uboot 启动以后进入命令行设置 bootcmd 环境变量的值。如果 EMMC 或者 NAND 中没有保存 bootcmd 的值，那么 uboot 就会使用默认的值，板子第一次运行 uboot 的时候都会使用默认值来设置 bootcmd 环境变量。bootcmd最终会调用bootz去执行内核和设备树。
>
>​	**bootcmd的默认值在mx6ull_cczhai_emmc.h中。** 
>
>1. 从EMMC启动Linux，bootcmd的命令为：
>
>~~~shell
>setenv bootcmd 'fatload mmc 1:1 80800000 zImage; fatload mmc 1:1 83000000 imx6ullalientek_emmc.dtb; bootz 80800000 - 83000000'
>savenev
>boot  
>~~~
>
>
>
>2. 使用tftp命令从网络启动Linux，bootcmd的命令为：
>
>~~~shell
>setenv bootcmd 'tftp 80800000 zImage; tftp 83000000 imx6ull-alientek-emmc.dtb; bootz 80800000 - 83000000'
>saveenv
>boot
>~~~
>
>

  booz范例：

    ~~~shell
    // 使用tftp和booz启动linux内核
    tftp 80800000 zImage
    tftp 83000000 imx6ull-alientek-emmc.dtb
    bootz 80800000 – 83000000   // addr为0x80800000，initrd用-表示，fdt为0x83000000
    ~~~

* 6. EMMC和SD卡操作命令

  |      |      |
  | ---- | ---- |
  |      |      |
  |      |      |
  |      |      |

    

* 7. FAT格式文件系统操作命令

  |      |      |
  | ---- | ---- |
  |      |      |
  |      |      |
  |      |      |

    

* 8. EXT格式文件系统操作命令

  |      |      |
  | ---- | ---- |
  |      |      |
  |      |      |
  |      |      |

* 9. 其他指令

  | 命令  | 功能                                                         |
  | ----- | ------------------------------------------------------------ |
  | reset | 复位重启                                                     |
  | go    | 跳到指定的地址处执行应用，如：<br/>tftp 87800000 printf.bin<br/>go 87800000 |
  | run   | 用于运行环境变量中定义的命令                                 |
  | mtest | DDR内存读写测试命令，mtest 80000000 80001000，结束测试就按下键盘上的“Ctrl+C”键 |

    

## 5.2 UBOOT目录分析

| 需要关心的目录/文件夹 | 功能                                                         |
| --------------------- | ------------------------------------------------------------ |
| arch                  | 与硬件架构有关的代码，Cortex-A7属于armv7，分析启动源码时，关注arch/cpu/armv7 |
| board                 | 不同开发板的代码,关注 board/freescale/mx6ullevk              |
| configs               | 配置文件，我们编译uboot时，使用配置文件对uboot配置，关注 configs/mx6ull_14x14_ddr512_emmc_defconfig |
| .u-boot.xxx_cmd 文件  |                                                              |
| Makefile              | 顶层makefile文件，该makefile会调用子目录的makefile           |
| u-boot.xxx 文件       | ├── u-boot      // 编译的ELF格式的uboot镜像文件<br/>├── u-boot.bin  // 编译出来的二进制格式的 uboot 可执行镜像文件<br/>├── u-boot.cfg<br/>├── u-boot.imx  // u-boot.bin 添加头部信息以后的文件， NXP 的 CPU 专用文件<br/>├── u-boot.lds  // 链接脚本<br/>├── u-boot.map  // uboot 映射文件，通过查看此文件可以知道某个函数被链接到了哪个地址上<br/>├── u-boot-nodtb.bin // 和u-boot.bin一样， u-boot.bin就是 u-boot-nodtb.bin 的复制文件<br/>├── u-boot.srec<br/>└── u-boot.sym |
| .config               | 编译生成的配置文件，通过查看该文件，可以看到uboot工程最后使能了哪个功能 |

## 5.3 UBOOT启动流程分析

### 5.3.1 程序入口

* 要分析uboot的启动流程，首先要找到程序入口，我们可以通过链接文件<font color=red>uboot.lds</font>找到程序入口，一般为：<font color=red>__start</font>;
* 






  <div style="page-break-after:always;"></div>  
# 第6章 驱动开发



  <div style="page-break-after:always;"></div>  
# 第7章 应用开发

