

你好

~~~shell
sudo apt-get install debootstrap -y
sudo apt-get install qemu-user-static -y
~~~



创建chroot_mount.sh

~~~shell
#!/bin/sh

sudo mount --bind /dev rootfs/dev/
sudo mount --bind /sys rootfs/sys/
sudo mount --bind /proc rootfs/proc/
sudo mount --bind /dev/pts rootfs/dev/pts/
~~~



创建chroot_unmount.sh

~~~shell
#!/bin/sh

sudo umount rootfs/sys/
sudo umount rootfs/proc/
sudo umount rootfs/dev/pts/
sudo umount rootfs/dev/
~~~

 创建chroot_run.sh

~~~shell
#!/bin/sh

sudo LC_ALL=C LANGUAGE=C LANG=C chroot rootfs
~~~



创建 sources.list

~~~shell
deb http://ftp2.cn.debian.org/debian buster main
~~~



